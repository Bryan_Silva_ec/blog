<?php

namespace App\Http\Controllers\Admin;

use App\Core\Entities\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\Repositories\CategoryRPY;
use Messages;

class CategoryController extends Controller
{
    
    private $objCategoryRPY; 

    function __construct(CategoryRPY $objCategoryRPY) {
        $this->objCategoryRPY = $objCategoryRPY;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $table = $this->objCategoryRPY->forTables($request);
       // return view('catalogos.categories.index', compact('table')); //compact indica como se va a llamar la variable en la vista
        return view('catalogos.categories.index')->with(['table'=>$table,'filter'=>$request->filter]);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('catalogos.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
        ['name'=>'required|unique:categories,name','description'=>'required'],
        ['name.required'=>'El nombre es obligatorio',
         'description.required'=>'La descripcion es obligatoria']);
         $this->objCategoryRPY->forSave($request);
         Messages::infoRegisterCustom('registro guardado correctamente');
         return redirect()->route('catalogos.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Core\Entities\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('catalogos.categories.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Core\Entities\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('catalogos.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Core\Entities\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request,
        ['name'=>'required|unique:categories,name','description'=>'required'],
        ['name.required'=>'El nombre es obligatorio',
         'description.required'=>'El description es obligatorio']);
        
        $this->objCategoryRPY->forUpdate($request,$category);
         Messages::infoRegisterCustom('registro actualizado correctamente');
         return redirect()->route('catalogos.categories.index');
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Core\Entities\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
       $category->delete();
       Messages::infoRegisterCustom('registro eliminado correctamente');
       return redirect()->route('catalogos.categories.index');
    }

    public function listSelect(Request $request) {
        if(!$request->ajaX)
        {
            return response()->json(Category::get(['name','id']));
                                     // pluck metodo de laravel utilizado para llenar los combos, que 
                                     // campos debe de tomar plug se alimenta de dos variables descripcion
                                    // la segunda repesenta el id
        }
        else {
            abort(401); //generar errores en laravel 
        }
    }

    public function dataTables() {
     
        return  datatables()->of(Category::all())->make(true);
    }

}
// php artisan make:controller --resource CategoryController --model:Categories
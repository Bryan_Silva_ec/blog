<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
// php artisan meke:request claseRequest 
// esta clase se encarga de hacer las validaciones 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //las reglas de validacion 
        return ['title'=>'required',
        'description'=>'required','categoria'=>'required',
        'picture'=>'required'
            //
        ];
    }
}

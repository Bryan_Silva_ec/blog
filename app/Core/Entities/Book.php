<?php

namespace App\Core\Entities;

use App\Events\BookEvent; // linea nueva 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopehelpers;


class Book extends Model  
{
    use SoftDeletes,  Sluggable, SluggableScopeHelpers;

    public function sluggable(){
        return [
            'slug' => [ 'source' => 'title']
        ];
    }

      protected $dispatchesEvents=[
        'creating' => BookEvent::class,
    ];


    protected $table ='books';
    protected $fillable = ['title','description','picture','category_id'];

    public function category(){
        return $this->belongsTo(Category::Class ,'category_id');
        
    }

    public function user(){
        return $this->belongsTo(\App\User::Class ,'user_id');
        
    }

    public function setPictureAttribute($value)
    {
        $this->attributes['picture'] = 'img-'.uniqid().uniqid().'.jpg';   
    }
}

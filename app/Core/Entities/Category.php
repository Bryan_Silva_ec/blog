<?php

namespace App\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    use SoftDeletes;
    protected $table ='categories'; // la tabla de la base de datos
    protected $fillable =['name','description']; //las variables que se muestran cuando se hacen consultas

    public function books()
    {
        return $this->hasMany(Book::Class , 'category_id'); //relacion entre tablas 
    }
}

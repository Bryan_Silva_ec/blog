<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Core\Entities\Book;
use App\Mail\BookMail;
use Illuminate\Contracts\Mail\Mailer; // ayuda para el envio de correo 
class sendBookEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $objBookMail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(BookMail $objBookMail)
    {
        $this->objBookMail = $objBookMail;
    }

   


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mailer->send($this->objBookMail);
    }
}

@component('mail::message')
# CREACION DE LIBROS 

Se publico un nuevo libro por el usuario {{$user}}, y lleva como titulo {{$title}}.

@component('mail::button', ['url' => '$path'])
VER LIBRO
@endcomponent

GRACIAS,<br>
{{ config('app.name') }}
@endcomponent

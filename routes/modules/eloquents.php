<?php


route::group(['prefix' => 'admin', 'as' => 'admin.'], function(){

    route::group(['middleware' => 'auth'], function(){

        route::get('categories-all',function(){
            return App\Core\Entities\Category::all();
        });
        
        route::get('load-data',function(){
            var_dump("cargando usuarios");
            factory(App\User::class,50)->create();
            var_dump("cargando categorias");
            factory(App\Core\Entities\Category::class,5)->create();
            var_dump("finalizacion con exito");
        });
        
        route::get('categories-{id}',function($id){
            return App\Core\Entities\Category::findOrFail($id); // si existe el elemento lo trae caso contrario da un error 405
        });
    });

    

});

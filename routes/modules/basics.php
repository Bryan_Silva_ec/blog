<?php
//rutas con parametros obligatorios
Route::get('/sumar/{operador1}/{operador2}',function($operador1,$operador2){
    //dd('sumar');// mata la ejecuccion 
    //var_dump(); crea un objeto
    return $operador1 + $operador2;
});

//rutas con parametros no obligatorios
Route::get('/sumar-opcional/{operador1}/{operador2?}',function($operador1,$operador2=0){
    //dd('sumar');// mata la ejecuccion 
    //var_dump(); crea un objeto
    return $operador1 + $operador2;
});

// rutas con validaciones 
Route::get('/array/{numero}',function($numero){
    
     $array=[];
     for($i=0;$i < $numero; $i++){
         $array[]=uniqid();
     }
        return response()->json($array);
})->where(['numero'=>'[0-9]+']); // expresion regular dentro de las rutas